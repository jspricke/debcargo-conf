Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: git-cinnabar
Upstream-Contact: Mike Hommey <mh@glandium.org>
Source: https://github.com/glandium/git-cinnabar

Files: *
Copyright: © 2014-2024, Mike Hommey <mh@glandium.org>
License: MPL-2.0

Files: git-core/reftable/*
Copyright: © 2020 Google LLC
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: git-core/compat/zlib-uncompress2.c
Copyright: © 1995-2003, 2010, 2014, 2016 Jean-loup Gailly, Mark Adler
License: Zlib
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not claim
    that you wrote the original software. If you use this software in a product,
    an acknowledgment in the product documentation would be appreciated but is
    not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: git-core/xdiff/*
Copyright: © 2003-2009, Davide Libenzi, Johannes E. Schindelin
License: LGPL-2.1+

Files: git-core/xdiff/xhistogram.c
Copyright: © 2010, Google Inc.
           and other copyright owners as documented in JGit's IP log.
License: EDL-1.0

Files: git-core/ewah/*
Copyright: © 2013, GitHub Inc.
           © 2009-2013, Daniel Lemire, Cliff Moon,
	       David McIntosh, Rober Becho, Google Inc. and Veronika Zenz
License: GPL-2+

Files: git-core/sha1dc/*
Copyright: © 2017,
	Marc Stevens
	Cryptology Group
	Centrum Wiskunde & Informatica
	P.O. Box 94079, 1090 GB Amsterdam, Netherlands
	marc@marc-stevens.nl
 .
	Dan Shumow
	Microsoft Research
	danshu@microsoft.com
License: Expat

Files: git-core/imap-send.c
Copyright: © 2000-2002, Michael R. Elkins <me@mutt.org>
           © 2002-2004, Oswald Buddenhagen <ossi@users.sf.net>
           © 2004, Theodore Y. Ts'o <tytso@mit.edu>
           © 2006, Mike McCormack
Name: git-imap-send - drops patches into an imap Drafts folder
  derived from isync/mbsync - mailbox synchronizer
License: GPL-2+

Files: git-core/kwset.c git-core/kwset.h
Copyright: © 1989, 1998, 2000, 2005, Free Software Foundation, Inc.
License: GPL-2+

Files: git-core/khash.h
Copyright: © 2008, 2009, 2011 by Attractive Chaos <attractor@live.co.uk>
License: Expat

Files: git-core/trace.c
Copyright: © 2000-2002, Michael R. Elkins <me@mutt.org>
           © 2002-2004, Oswald Buddenhagen <ossi@users.sf.net>
           © 2004, Theodore Y. Ts'o <tytso@mit.edu>
           © 2006, Mike McCormack
           © 2006, Christian Couder
License: GPL-2+

Files: git-core/sh-i18n--envsubst.c
Copyright: © 2010, Ævar Arnfjörð Bjarmason
           © 1998-2007, Free Software Foundation, Inc.
License: GPL-2+

Files: git-core/compat/inet_ntop.c git-core/compat/inet_pton.c
Copyright: © 1996-2001, Internet Software Consortium.
License: ISC

Files: git-core/compat/poll/poll.c git-core/compat/poll/poll.h
Copyright: © 2001-2003, 2006-2011, Free Software Foundation, Inc.
Name: Emulation for poll(2) from gnulib.
License: GPL-2+

Files: git-core/compat/vcbuild/include/sys/utime.h
Copyright: ?
License: mingw-runtime

Files: git-core/compat/nedmalloc/*
Copyright: © 2005-2006 Niall Douglas
License: Boost

Files: git-core/compat/nedmalloc/malloc.c.h
Copyright: © 2006, KJK::Hyperion <hackbunny@reactos.com>
License: dlmalloc

Files: git-core/compat/regex/*
Copyright: © 1985, 1989-93, 1995-2010, Free Software Foundation, Inc.
Name: Extended regular expression matching and search library
License: LGPL-2.1+

Files: git-core/compat/obstack.c git-core/compat/obstack.h
Copyright: © 1988-1994, 1996-2005, 2009, Free Software Foundation, Inc.
Name: Object stack macros.
License: LGPL-2.1+

License: MPL-2.0
 The complete text of the Mozilla Public License 2.0 can be found in
 the file `/usr/share/common-licenses/MPL-2.0'.

License: GPL-2
 You can redistribute this software and/or modify it under the terms of
 the GNU General Public License as published by the Free Software
 Foundation; version 2 dated June, 1991.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Library General Public License
 can be found in the /usr/share/common-licenses/LGPL-2 file.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in /usr/share/common-licenses/LGPL-2.1.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

License: Expat
 <http://www.opensource.org/licenses/mit-license.php>:
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: EDL-1.0
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the following
 conditions are met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.
 .
 - Neither the name of the Eclipse Foundation, Inc. nor the
   names of its contributors may be used to endorse or promote
   products derived from this software without specific prior
   written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: mingw-runtime
 This file has no copyright assigned and is placed in the Public Domain.
 This file is a part of the mingw-runtime package.
 .
 The mingw-runtime package and its code is distributed in the hope that it
 will be useful but WITHOUT ANY WARRANTY.  ALL WARRANTIES, EXPRESSED OR
 IMPLIED ARE HEREBY DISCLAIMED.  This includes but is not limited to
 warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 .
 You are free to use this package and its code without limitation.

License: Boost
 It is licensed under the Boost Software License which basically means
 you can do anything you like with it. This does not apply to the malloc.c.h
 file which remains copyright to others.
 .
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: dlmalloc
 This is a version (aka dlmalloc) of malloc/free/realloc written by
 Doug Lea and released to the public domain, as explained at
 http://creativecommons.org/licenses/publicdomain.  Send questions,
 comments, complaints, performance data, etc to dl@cs.oswego.edu
 .
 Incorporates code from intrin_x86.h, which bears the following notice:
 .
 Compatibility <intrin_x86.h> header for GCC -- GCC equivalents of intrinsic
 Microsoft Visual C++ functions. Originally developed for the ReactOS
 (<http://www.reactos.org/>) and TinyKrnl (<http://www.tinykrnl.org/>)
 projects.
 .
 Copyright (c) 2006 KJK::Hyperion <hackbunny@reactos.com>
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
